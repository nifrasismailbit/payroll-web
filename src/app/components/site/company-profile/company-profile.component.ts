import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormControl, FormArray } from '@angular/forms';
import { HttpClient, HttpParams } from '@angular/common/http';
import { ApiService } from '../../../api.service';
import Swal from 'sweetalert2';
import { Router, ActivatedRoute } from "@angular/router";

@Component({
  selector: 'app-company-profile',
  templateUrl: './company-profile.component.html',
  styleUrls: ['./company-profile.component.css']
})
export class CompanyProfileComponent implements OnInit {
  company_id  :any;
  company_detail : any;
  constructor(private apiService: ApiService, private formBuilder: FormBuilder, private Activatedroute: ActivatedRoute) { }

  ngOnInit() {

    this.company_id = this.Activatedroute.snapshot.params['id'];

    this.apiService.getCompany(this.company_id)
    .subscribe(data=>{
      this.company_detail = data;
    });
  }

}
