import { Component, OnInit } from '@angular/core';
import { HttpClient,HttpErrorResponse } from '@angular/common/http';
import { environment } from '../../../../environments/environment';
import {Router} from "@angular/router";
import { ApiService } from '../../../api.service';
import Swal from 'sweetalert2';
import { FormControl, FormGroup, FormBuilder, Validators, NgForm, FormArray } from '@angular/forms';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  loginf:any = {};
  submitted =false;
  constructor(private formBuilder: FormBuilder,private apiService: ApiService,private http:HttpClient,private router: Router) { }
  LoginForm : FormGroup;
  ngOnInit() {

    this.LoginForm = this.formBuilder.group({    
      email: ['', Validators.compose([Validators.required,
        Validators.pattern('^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+.[a-zA-Z0-9-.]+$')
      ])], 
      password: ['', [Validators.required,Validators.minLength(6)]]
         });
  }

  get f() { return this.LoginForm.controls; }

  createEmployee(){
    this.submitted = true;
    if (this.LoginForm.invalid) {
      return;
    }
   this.apiService.login(this.LoginForm.value)
       .subscribe(data=>{
      
         localStorage.setItem ('token', data['access_token']);
         localStorage.setItem('loogedIn', 'true');
         localStorage.setItem('role', data['user']['roles'][0]['id']);
         localStorage.setItem('details', JSON.stringify( data['user']));
         this.router.navigate(['/platform/home']);
       },
   
     error => {
      Swal.fire(
        'error',
        'Please enter valid credentials!',
        'error'
      )
    
       //this.toastr.error(error);
      });
       
     }

}
