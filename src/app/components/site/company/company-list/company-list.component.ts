import { Component, OnInit } from '@angular/core';
import { HttpClient,HttpErrorResponse } from '@angular/common/http';
import { environment } from '../../../../../environments/environment';
import {Router} from "@angular/router";
import { ApiService } from '../../../../api.service';
import Swal from 'sweetalert2';
import { FormControl, FormGroup, FormBuilder, Validators, NgForm, FormArray } from '@angular/forms';

@Component({
  selector: 'app-company-list',
  templateUrl: './company-list.component.html',
  styleUrls: ['./company-list.component.css']
})
export class CompanyListComponent implements OnInit {
  company_details : any;
  constructor(private formBuilder: FormBuilder,private apiService: ApiService,private http:HttpClient,private router: Router) { }

  ngOnInit() {

    this.getCompanyDetails();
  }

  getCompanyDetails()
  {
    this.apiService.getCompanyList()
    .subscribe(data=>{
      this.company_details = data;
    });
  }

  profile(id)
  {
    this.router.navigate(['/platform/company-profile',id]);
  }

}
