import { Component, OnInit } from '@angular/core';
import {Router} from "@angular/router";
import { ApiService } from '../../../api.service';
@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {
  obj:any = {};
  constructor(private apiService: ApiService,private router: Router) { }

  ngOnInit() {
  }
  profile()
  {
    this.router.navigate(['/platform/profile']);
  }

  logout() {
     this
     .apiService
     .logOut(this.obj)
     .subscribe(data => {
       localStorage.clear();
       this.router.navigate(['']);
     });
 
 
   }
}
