import { Injectable, Injector } from '@angular/core';
import { HttpRequest, HttpHandler, HttpEvent, HttpInterceptor } from '@angular/common/http';
import { ApiService } from './api.service';
import { catchError } from "rxjs/internal/operators";


import { Observable, of } from 'rxjs';
import { Router, ActivatedRoute } from '@angular/router';
import Swal from 'sweetalert2';

@Injectable({
  providedIn: 'root'
})
export class TokenInterceptorService implements HttpInterceptor {
tokenizedReq: any;
// message: any;
// error:any;
  constructor(private apiService: ApiService, private injector: Injector, ) { }

//   intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
// // let apiService = this.injector.get(ApiService)
// request = request.clone({setHeaders: { Authorization: 'Bearer ' + (localStorage.getItem('token')) } });
// return next.handle(request);
// // return next.handle(request);
// }

intercept(req, next) {

  let tokenizedReq = req.clone({
    setHeaders: {
      Authorization: 'Bearer ' + (localStorage.getItem('token'))
      //Authorization : 'Bearer '
    }


  })
  // return next.handle(tokenizedReq);

  return next.handle(tokenizedReq).pipe(catchError((error, caught) => {
    //intercept the respons error and displace it to the console
   // console.log(error);

  Swal.fire(
    'error',
    error.error.message,
    'error'
  )
    //this.handleAuthError(error);
    return of(error.error.message);
  }) as any);



}


}


