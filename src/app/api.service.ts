import { Injectable } from '@angular/core';
import { HttpClient, HttpClientModule } from '@angular/common/http';
import { environment } from '../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ApiService {

  constructor(private http: HttpClient) { }

  localUrl = environment.baseUrl;
  login(obj) {
    
    return this.http.post(this.localUrl + `/user/login`, obj);
  }

  logOut(obj) {
    
    return this.http.post(this.localUrl + `/user/logout`, obj);
  }

  
  getCompanyList() {
    
    return this.http.get(this.localUrl + `/company`);
  }

  getCompany(id) {
    
    return this.http.get(this.localUrl + `/company/`+ id);
  }
  
}
