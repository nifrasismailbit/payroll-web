import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { ApiService } from './api.service';
import { AppComponent } from './app.component';
import {RouterModule, Routes} from "@angular/router";
import { HeaderComponent } from './components/layout/header/header.component';
import { SidemenuComponent } from './components/layout/sidemenu/sidemenu.component';
import { FooterComponent } from './components/layout/footer/footer.component';
import {HomeComponent} from "./components/site/home/home.component";
import { AllEmployeesComponent } from './components/site/employees/all-employees/all-employees.component';
import { AllEmployeesListComponent } from './components/site/employees/all-employees-list/all-employees-list.component';
import { ProfileComponent } from './components/site/employees/profile/profile.component';
import { CompanyListComponent } from './components/site/company/company-list/company-list.component';
import { LoginComponent } from './components/site/login/login.component';
import { DashboardComponent } from './components/site/dashboard/dashboard.component';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { HttpClientModule, HTTP_INTERCEPTORS} from '@angular/common/http';
import { TokenInterceptorService } from './token-interceptor.service';
import { CompanyProfileComponent } from './components/site/company-profile/company-profile.component';
const appRoutes: Routes = [
  {path: '' , component: LoginComponent},
  {path: 'platform', component: DashboardComponent,
  children: [
    {path: 'home' , component: HomeComponent},
    {path: 'profile' , component: ProfileComponent},
    {path: 'employees' , component: AllEmployeesComponent},
    {path: 'company-list' , component: CompanyListComponent},
    {path: 'company-profile/:id' , component: CompanyProfileComponent}
  ]
  }

  
  // {path: 'employees' , component: AllEmployeesComponent},
  // {path: 'employees-list' , component: AllEmployeesListComponent},
  // {path: 'company-list' , component: CompanyListComponent},
  // {path: 'profile' , component: AllEmployeesListComponent}
]

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    HeaderComponent,
    SidemenuComponent,
    FooterComponent,
    AllEmployeesComponent,
    AllEmployeesListComponent,
    ProfileComponent,
    CompanyListComponent,
    LoginComponent,
    DashboardComponent,
    CompanyProfileComponent
  ],
  imports: [
    BrowserModule,
    RouterModule.forRoot(appRoutes),
    ReactiveFormsModule, FormsModule,
    HttpClientModule,
  ],
  providers: [ApiService, {
    provide: HTTP_INTERCEPTORS,
    useClass: TokenInterceptorService,
    multi: true
  }],
  bootstrap: [AppComponent]
})
export class AppModule { }
